<?php

namespace Drupal\dmt\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Module entities.
 *
 * @ingroup dmt
 */
class ModuleDeleteForm extends ContentEntityDeleteForm {


}
