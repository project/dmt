<?php

namespace Drupal\dmt\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Weekly usage entities.
 *
 * @ingroup dmt
 */
class WeeklyUsageDeleteForm extends ContentEntityDeleteForm {


}
