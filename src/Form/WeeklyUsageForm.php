<?php

namespace Drupal\dmt\Form;

use Drupal\Core\Entity\ContentEntityForm;

/**
 * Form controller for Weekly usage edit forms.
 *
 * @ingroup dmt
 */
class WeeklyUsageForm extends ContentEntityForm {

}
